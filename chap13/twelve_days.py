#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-09
Purpose: Rock the Casbah
"""
import sys
import argparse
import os


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-n',
                        '--num',
                        help='Number of days to sing',
                        metavar='days',
                        type=int,
                        default=12)

    parser.add_argument('-o',
                        '--outfile',
                        help='Outfile (STOUT)',
                        metavar='FILE',
                        type=argparse.FileType('wt'),
                        default=sys.stdout)

    args = parser.parse_args()
    num_arg = args.num
    if num_arg < 1 or num_arg > 12:
        parser.error(f'--num "{num_arg}" must be between 1 and 12')

    return args

# --------------------------------------------------


def main():
    """Make a jazz noise here"""

    args = get_args()
    print_song(args.num, args.outfile)


def print_song(num, outfile):
    verses = map(get_verse, range(1, num+1))
    result = '\n\n'.join(verses)
    print(result, file=outfile)


def get_verse(idx):
    day = get_day(idx)
    gifts = get_gifts(idx)
    verse = f'On the {day} day of Christmas,\n'
    verse += 'My true love gave to me,\n'
    len_gifts = len(gifts)
    if len_gifts == 1:
        verse += gifts[0] + '.'
    else:
        verse += ',\n'.join(gifts[:len_gifts-1])
        verse += f',\nAnd {gifts[-1].lower()}.'
    return verse


def get_day(idx) -> str:
    ordinal_list = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth',
                    'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth']
    if idx < 0 or idx > len(ordinal_list):
        return 'unknown'
    return ordinal_list[idx-1]


def get_gifts(idx):

    gifts = ['A partridge in a pear tree',
             'Two turtle doves',
             'Three French hens',
             'Four calling birds',
             'Five gold rings',
             'Six geese a laying',
             'Seven swans a swimming',
             'Eight maids a milking',
             'Nine ladies dancing',
             'Ten lords a leaping',
             'Eleven pipers piping',
             'Twelve drummers drumming'
             ]
    return list(reversed(gifts[:idx]))


# --------------------------------------------------
if __name__ == '__main__':
    main()
