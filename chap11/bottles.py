#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-09
Purpose: Rock the Casbah
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-n',
                        '--num',
                        help='Number of bottles',
                        metavar='number',
                        type=int,
                        default='10')

    args = parser.parse_args()
    num_bottles = args.num
    if num_bottles <= 0:
        parser.error(f'--num "{num_bottles}" must be greater than 0')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """Make a jazz noise here"""

    args = get_args()
    print_verse(args.num)


def print_verse(num_bottles):
    template = '{} bottle{} of beer on the wall,\n{} bottle{} of beer,\nTake one down, pass it around,'
    bottles_left_template = '{} bottle{} of beer on the wall!\n'
    no_bottles_template = 'No more bottles of beer on the wall!'

    while num_bottles >= 1:
        print(template.format(num_bottles, 's' if num_bottles >
                              1 else '', num_bottles, 's' if num_bottles > 1 else ''))
        if num_bottles > 1:
            print(bottles_left_template.format(
                num_bottles-1, 's' if num_bottles-1 > 1 else ''))
        else:
            print(no_bottles_template)

        num_bottles -= 1


# --------------------------------------------------
if __name__ == '__main__':
    main()
