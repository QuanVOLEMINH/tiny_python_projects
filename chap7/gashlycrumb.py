#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-09
Purpose: Rock the Casbah
"""

import argparse
import os

# --------------------------------------------------


def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('letter',
                        metavar='LETTER',
                        nargs='+',
                        help='Letter(s)')
    parser.add_argument('-f',
                        '--file',
                        help='A file to read',
                        metavar='FILE',
                        type=argparse.FileType('r'),
                        default='gashlycrumb.txt')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """ """
    args = get_args()
    letter_arg = args.letter
    file_arg = args.file
    look_up_then_print(letter_arg, file_arg)


def look_up_then_print(letters, target_fh):
    gashly_table = dict()

    for line in target_fh:
        first_char = line.split()[0].lower()
        if len(first_char) == 1:
            gashly_table[first_char] = line.strip()

    for l in letters:
        ll = l.lower()
        if ll in gashly_table:
            print(gashly_table[ll])
        else:
            print(f'I do not know "{l}".')


# --------------------------------------------------
if __name__ == '__main__':
    main()
