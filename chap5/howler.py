#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-09
Purpose: Rock the Casbah
"""

import argparse
import os

# --------------------------------------------------


def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('text',
                        metavar='str',
                        help='A text or a path to a file that contains text')

    parser.add_argument('-o',
                        '--outfile',
                        help='Output file path',
                        metavar='str',
                        type=str,
                        default='')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """ Be patient """

    args = get_args()
    text = args.text
    outfile = args.outfile

    take_howler_action(text, outfile)


def take_howler_action(text, outfile):

    if os.path.isfile(text):
        # read file
        message = open(text).read().rstrip()
    else:
        message = text

    result = message.upper()

    if outfile:
        # print to file
        fd = os.open(outfile, os.O_CREAT | os.O_RDWR)
        os.write(fd, result.encode())
        os.close(fd)
    else:
        print(result)


# --------------------------------------------------
if __name__ == '__main__':
    main()
