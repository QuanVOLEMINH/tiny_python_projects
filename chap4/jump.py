#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-09
Purpose: Rock the Casbah
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('message',
                        metavar='message',
                        help='A message to encrypt')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """Make a jazz noise here"""

    args = get_args()
    plain_message = args.message

    print(get_encrypted_message(plain_message))


def get_encrypted_message(message) -> str:
    encr_dict = get_encr_dict()
    encr_message = ''
    for c in message:
        if c.isdigit():
            encr_message += str(encr_dict[int(c)])
            continue
        encr_message += c

    return encr_message


def get_encr_dict() -> dict:
    encr_dict = {0: 5, 5: 0}
    for i in range(1, 10):
        if i not in encr_dict:
            encr_dict[i] = 10-i
    return encr_dict


# --------------------------------------------------
if __name__ == '__main__':
    main()
