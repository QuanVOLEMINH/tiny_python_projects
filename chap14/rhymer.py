#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-15
Purpose: Rock the Casbah
"""

import argparse
import re
import string

# --------------------------------------------------


def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rhythm in the rain',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('word',
                        metavar='word',
                        help='A word to rhyme')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """Make a jazz noise here"""

    args = get_args()
    rhyme(args.word)


def rhyme(word):
    first, sec = stemmer(word)
    if sec == '':
        print(f'Cannot rhyme "{word}"')
    else:
        conso_list = get_consonants() + get_conso_combi()
        for c in sorted(conso_list):
            if c == first:
                continue
            print(c+sec)


def stemmer(word):
    word = word.lower()
    consonants = '|'.join(get_consonants())
    my_re = f'^[{consonants}|0-9]*'
    m = re.match(my_re, word)
    last_pos = m.span()[1]
    return (m.group(), word[last_pos:])


def get_consonants():
    return [c for c in string.ascii_lowercase if c not in 'aeiou']


def get_conso_combi():
    return 'bl br ch cl cr dr fl fr gl gr pl pr sc sh sk sl sm sn sp st sw th tr tw thw wh wr sch scr shr sph spl spr squ str thr'.split()


# --------------------------------------------------
if __name__ == '__main__':
    main()
