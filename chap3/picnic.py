#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-08
Purpose: Go picnic
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('items', nargs='+',
                        help='Items which you want to bring')

    parser.add_argument('-s', '--sorted', action='store_true')

    return parser.parse_args()

# --------------------------------------------------


def main():
    """ Just a main """

    args = get_args()
    print(get_picnic_str(args.items, args.sorted))


def get_picnic_str(items, sort):
    template = 'You are bringing {}.'
    l = len(items)
    if l == 0:
        item_list = 'nothing'
    elif l == 1:
        item_list = items[0]
    else:
        sorted_items = sorted(items) if sort else items
        first_part = sorted_items[:l-1]
        item_list = ', '.join(first_part)
        if len(first_part) > 1:
            item_list += ','
        item_list += ' and {}'.format(sorted_items[l-1])

    return template.format(item_list)


# --------------------------------------------------
if __name__ == '__main__':
    main()
