#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-15
Purpose: Rock the Casbah
"""

import argparse
import os
import re
import random

# --------------------------------------------------


def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('text',
                        metavar='str',
                        help='Input text or file')

    parser.add_argument('-s',
                        '--seed',
                        help='Random seed',
                        metavar='seed',
                        type=int,
                        default=None)

    args = parser.parse_args()
    text_or_file = args.text

    if os.path.isfile(text_or_file):
        args.text = open(text_or_file).read().rstrip()

    return args


# --------------------------------------------------
def main():
    """Make a jazz noise here"""

    args = get_args()
    scrambler(args.text, args.seed)


def scrambler(text, seed):
    random.seed(seed)
    for line in text.splitlines():
        words = []
        for w in re.split('([\s|,|.]+)', line):
            if len(w) > 3:
                x = list(w)
                y = x[1:-1]
                random.shuffle(y)
                words.append(x[0]+''.join(y)+x[-1])
            else:
                words.append(w)
        print(''.join(words))


# --------------------------------------------------
if __name__ == '__main__':
    main()
