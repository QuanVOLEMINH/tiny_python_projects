#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-09
Purpose: Rock the Casbah
"""

import os
import argparse
import random


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('text',
                        metavar='text',
                        help='Input text')

    parser.add_argument('-s',
                        '--seed',
                        help='Seed',
                        metavar='int',
                        type=int,
                        default=None)
    args = parser.parse_args()

    if os.path.isfile(args.text):
        args.text = open(args.text).read().rstrip()

    return args


# --------------------------------------------------
def main():
    """Make a jazz noise here"""

    args = get_args()
    print(get_ransom_text(args.text, args.seed))


def get_ransom_text(text, seed):
    random.seed(seed)
    result = ''
    for c in text:
        result += c.upper() if random.choice([0, 1]) else c.lower()

    return result

    # --------------------------------------------------
if __name__ == '__main__':
    main()
