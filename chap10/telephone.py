#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-09
Purpose: Rock the Casbah
"""
import os
import argparse
import random
import string


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('str',
                        metavar='str',
                        help='Input text or file')

    parser.add_argument('-s',
                        '--seed',
                        help='Random seed',
                        metavar='int',
                        type=int,
                        default=None)

    parser.add_argument('-m',
                        '--mutations',
                        help='Percent mutations',
                        metavar='int',
                        type=float,
                        default=0.1)
    args = parser.parse_args()
    mutations_arg = args.mutations

    if mutations_arg < 0 or mutations_arg > 1:
        parser.error(f'--mutations "{mutations_arg}" must be between 0 and 1')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """Make a jazz noise here"""

    args = get_args()
    telephone_mutations(args.str, args.seed, args.mutations)


def telephone_mutations(message, seed, mutations):
    if os.path.isfile(message):
        content = open(message, 'r').read().rstrip()
    else:
        content = message
    random.seed(seed)
    you_template = 'You said: "{}"'
    me_template = 'I heard : "{}"'

    print(you_template.format(content))
    alpha = ''.join(sorted(string.punctuation + string.ascii_letters))
    len_text = len(content)
    num_mutations = round(mutations*len_text)
    new_text = content

    for i in random.sample(range(len_text), num_mutations):
        new_char = random.choice(alpha.replace(new_text[i], ''))
        new_text = new_text[:i] + new_char + new_text[i+1:]
    print(me_template.format(new_text))


# --------------------------------------------------
if __name__ == '__main__':
    main()
