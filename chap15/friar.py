#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-15
Purpose: Rock the Casbah
"""

import argparse
import os
import re

# --------------------------------------------------


def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('text',
                        metavar='str',
                        help='Input text or file')
    args = parser.parse_args()

    text_or_file = args.text

    if os.path.isfile(text_or_file):
        args.text = open(text_or_file).read().rstrip()

    return args

# --------------------------------------------------


def main():
    """Make a jazz noise here"""

    args = get_args()
    friar(args.text)


def friar(text):
    for line in text.splitlines():
        words = []
        for w in re.split('(\W+)', line.rstrip()):
            words.append(fry(w))
        print(''.join(words))


def fry(word):
    you = re.match('([yY])ou$', word)

    ing = re.search('(.+)ing$', word)

    if you:
        return you.group(1) + "'all"
    elif ing:
        first = ing.group(1)
        if re.search('[aeiou]', first, re.IGNORECASE):
            return first + "in'"
    return word

    # --------------------------------------------------
if __name__ == '__main__':
    main()
