#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-09
Purpose: Rock the Casbah
"""

import argparse
import sys
import re


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('file',
                        metavar='FILE',
                        nargs='*',
                        type=argparse.FileType('r'),
                        default=[sys.stdin],
                        help='Input files')

    return parser.parse_args()

# --------------------------------------------------


def main():
    """Make a jazz noise here"""

    args = get_args()
    word_count(args.file)


def word_count(files):
    tlines = twords = tbytes = 0
    for fh in files:
        nlines = nwords = nbytes = 0
        for line in fh:
            nlines += 1
            nwords += len(line.split())
            nbytes += len(line)
        print(f'{nlines:8}{nwords:8}{nbytes:8} {fh.name}')

        tlines += nlines
        twords += nwords
        tbytes += nbytes

    if len(files) > 1:
        print(f'{tlines:8}{twords:8}{tbytes:8} total')


# --------------------------------------------------
if __name__ == '__main__':
    main()
