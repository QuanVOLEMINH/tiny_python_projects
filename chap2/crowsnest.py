#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-08
Purpose: Crow's Nest 
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Crow\'s Nest -- choose the correct article',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('word',
                        metavar='word',
                        help='A word')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """Make a jazz noise here"""

    args = get_args()
    word = args.word

    print(get_sentence(word))


def get_sentence(word) -> str:
    template = 'Ahoy, Captain, {} {} off the larboard bow!'
    first_char = word[0].lower()
    if first_char in 'aeiou':
        article = 'an'
    else:
        article = 'a'
    return template.format(article, word)


# --------------------------------------------------
if __name__ == '__main__':
    main()
