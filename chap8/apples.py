#!/usr/bin/env python3
"""
Author : quanvoleminh <quanvoleminh@localhost>
Date   : 2020-08-09
Purpose: Rock the Casbah
"""

import argparse
import os

# --------------------------------------------------


def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('text',
                        metavar='text',
                        help='Input text')

    parser.add_argument('-v',
                        '--vowel',
                        help='Vowel to replace',
                        metavar='str',
                        type=str,
                        choices=['a', 'e', 'i', 'o', 'u'],
                        default='a')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """Make a jazz noise here"""

    args = get_args()
    print(apples_replace(args.text, args.vowel))


def apples_replace(text, vowel):
    if os.path.isfile(text):
        content = open(text, 'r').read()
    else:
        content = text
    result = ''
    for c in content:
        if c in 'aeiou':
            result += vowel.lower()
        elif c in 'AEIOU':
            result += vowel.upper()
        else:
            result += c
    return result


# --------------------------------------------------
if __name__ == '__main__':
    main()
